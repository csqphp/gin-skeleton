package bootstrap

import (
	"chongwu/app/library/global"
	"chongwu/utils/snow_flake"
)

func Init() {
	global.SnowFlake = snow_flake.CreateSnowflakFactory()
}
