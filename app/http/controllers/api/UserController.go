package api

import (
	"chongwu/app/library/consts"
	"chongwu/app/models"
	"chongwu/utils/response"
	"github.com/gin-gonic/gin"
	"log"
	"strconv"
)

func GetUserFirstId(c *gin.Context) {
	id := (&models.UserModel{}).GetUserFirstId()

	if id != 0 {
		response.Success(c, consts.CurdStatusOkMsg, id)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}

func GetUserById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Fatal(err)
	}

	res := (&models.UserModel{}).GetUserById(uint(id))
	if res != nil {
		response.Success(c, consts.CurdStatusOkMsg, res)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}

func GetUserLastId(c *gin.Context) {
	id := (&models.UserModel{}).GetUserLastId()

	if id != 0 {
		response.Success(c, consts.CurdStatusOkMsg, id)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}
