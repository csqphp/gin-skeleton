package api

import (
	"chongwu/app/library/consts"
	"chongwu/app/library/global"
	"chongwu/app/models"
	"chongwu/utils/response"
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetTestFirstId(c *gin.Context) {
	id := (&models.TestModel{}).GetTestFirstId()

	if id != 0 {
		response.Success(c, consts.CurdStatusOkMsg, id)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}

func GetTestById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		global.ZapLog.Error(err.Error())
	}

	TestModel := &models.TestModel{}
	res := TestModel.GetTestById(uint(id))
	if res != nil {
		response.Success(c, consts.CurdStatusOkMsg, res)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}

func GetTestLastId(c *gin.Context) {
	id := (&models.TestModel{}).GetTestLastId()

	if id != 0 {
		response.Success(c, consts.CurdStatusOkMsg, id)
	} else {
		response.Fail(c, consts.CurdSelectFailCode, consts.CurdSelectFailMsg, "")
	}
}
