package my_errors

const (
	// 启动
	ErrorBasePath   string = "初始化项目根目录失败"
	ErrorLoadConfig string = "初始化加载config失败"

	ErrorDbDriverNotExists   string = "数据库驱动类型不存在,目前支持的数据库类型：mysql、sqlserver、postgresql，您提交数据库类型："
	ErrorDialectorDbInitFail string = "gorm dialector 初始化失败,dbType:"
	ErrorConnectDbFail       string = "gorm db connect 初始化失败,err:"

	ErrorsNoAuthorization string = "token鉴权未通过，请通过token授权接口重新获取token,"

	//服务器代码发生错误
	ServerOccurredErrorCode int    = -500100
	ServerOccurredErrorMsg  string = "服务器内部发生代码执行错误, "
)
