package global

import (
	"chongwu/app/library/my_errors"
	"chongwu/utils/snow_flake/snowflake_interf"
	"go.uber.org/zap"
	"log"
	"os"
	"strings"
)

var (
	BasePath string // 全局根目录路径

	//雪花算法全局变量
	SnowFlake snowflake_interf.InterfaceSnowFlake

	// 全局日志指针
	ZapLog *zap.Logger
)

func Init() {
	// 1.初始化程序根目录
	if path, err := os.Getwd(); err != nil {
		log.Fatal(my_errors.ErrorBasePath, err)
	} else {
		BasePath = strings.TrimRight(strings.Replace(strings.Replace(path, "/api", "", 1), "/cmd", "", 1), "/") + "/"
	}
}
