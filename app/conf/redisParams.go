package conf

type RedisParams struct {
	Addr               string
	Auth               string
	MaxIdle            int
	MaxActive          int
	IdleTimeout        int
	IndexDb            int
	ConnFailRetryTimes int
	ReConnectInterval  int
}
