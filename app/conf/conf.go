package conf

import (
	"chongwu/app/library/global"
	"chongwu/app/library/my_errors"
	"github.com/BurntSushi/toml"
	"log"
)

var (
	Conf      = &Config{} // config 配置
	DbConf    = map[string]map[string]DbParams{}
	RedisConf = map[string]RedisParams{}
)

type Config struct {
	AppDebug           bool
	HttpServer         map[string]*serve
	SnowFlakeMachineId int64
}

type serve struct {
	Port string
}

func Init() {
	if _, err := toml.DecodeFile(global.BasePath+"config/config.toml", &Conf); err != nil {
		log.Fatal(my_errors.ErrorLoadConfig, err)
	}

	if _, err := toml.DecodeFile(global.BasePath+"config/db.toml", &DbConf); err != nil {
		log.Fatal(my_errors.ErrorLoadConfig, err)
	}

	if _, err := toml.DecodeFile(global.BasePath+"config/redis.toml", &RedisConf); err != nil {
		log.Fatal(my_errors.ErrorLoadConfig, err)
	}
}

func GetAppConf(path string, conf interface{}) (err error) {
	_, err = toml.DecodeFile(global.BasePath+path, conf)
	return
}
