package conf

type DbParams struct {
	Host         string
	Port         int64
	Username     string
	Password     string
	Database     string
	DatabaseType string
	Charset      string
}
