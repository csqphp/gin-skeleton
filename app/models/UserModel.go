package models

type UserModel struct {
	BaseModel `json:"-"`
	Id        uint   `gorm:"primarykey" json:"id"`
	Name      string `gorm:"column:name" json:"name"`
	Avatar    string `gorm:"column:avatar" json:"avatar"`
	CreatedAt DBTime `json:"created_at"`
	UpdatedAt DBTime `json:"updated_at"`
}

func (u *UserModel) new() *UserModel {
	return &UserModel{BaseModel:BaseModel{DataBase: "test1"}}
}

// 表名
func (u *UserModel) TableName() string {
	return "user"
}

func (u *UserModel) GetUserFirstId() uint {
	if result := u.new().Model(false).First(u); result.RowsAffected > 0 {
		return u.Id
	} else {
		return 0
	}
}

func (u *UserModel) GetUserLastId() uint {
	if result := u.new().Model(false).Last(u); result.RowsAffected > 0 {
		return u.Id
	} else {
		return 0
	}
}

func (u *UserModel) GetUserById(id uint) *UserModel {
	if result := u.new().Model(false).Where("id = ?", id).First(u); result.RowsAffected > 0 {
		return u
	} else {
		return nil
	}
}
