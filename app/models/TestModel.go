package models

type TestModel struct {
	BaseModel `json:"-"`
	Id        uint   `gorm:"primarykey" json:"id"`
	Title     string `gorm:"column:title" json:"title"`
	CreatedAt DBTime `json:"created_at"`
	UpdatedAt DBTime `json:"updated_at"`
}


func (t *TestModel) new() *TestModel {
	return &TestModel{BaseModel:BaseModel{DataBase: "test"}}
}

// 表名
func (t *TestModel) TableName() string {
	return "test"
}

func (t *TestModel) GetTestFirstId() uint {
	if result := t.new().Model(false).First(t); result.RowsAffected > 0 {
		return t.Id
	} else {
		return 0
	}
}

func (t *TestModel) GetTestLastId() uint {
	if result := t.new().Model(false).Last(t); result.RowsAffected > 0 {
		return t.Id
	} else {
		return 0
	}
}

func (t *TestModel) GetTestById(id uint) *TestModel {
	if result := t.new().Model(false).Where("id = ?", id).First(t); result.RowsAffected > 0 {
		return t
	} else {
		return nil
	}
}
