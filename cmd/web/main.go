package main

import (
	"chongwu/app/conf"
	"chongwu/app/library/global"
	"chongwu/bootstrap"
	"chongwu/routers"
)

func main() {
	global.Init()
	conf.Init()
	bootstrap.Init()

	router := routers.InitWebRouter()
	router.Run(conf.Conf.HttpServer["api"].Port)
}
