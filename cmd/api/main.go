package main

import (
	"chongwu/app/conf"
	"chongwu/app/library/global"
	"chongwu/bootstrap"
	"chongwu/routers"
)

func main() {
	global.Init()
	conf.Init()
	bootstrap.Init()
	//fmt.Println(conf.Conf.HttpServer["api"].Port)
	//fmt.Println(conf.DbConf)
	//fmt.Println(conf.RedisConf["master"].Host)

	router := routers.InitApiRouter()
	router.Run(conf.Conf.HttpServer["api"].Port)
}
