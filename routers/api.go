package routers

import (
	"chongwu/app/http/controllers/api"
	"github.com/gin-gonic/gin"
	"net/http"
)

func InitApiRouter() *gin.Engine {
	var router *gin.Engine
	router = gin.Default()

	apiGroup := router.Group("/api")
	{
		testGroup := apiGroup.Group("/test")
		{
			testGroup.GET("/:id", api.GetTestById)
		}
		UserGroup := apiGroup.Group("/user")
		{
			UserGroup.GET("/:id", api.GetUserById)
		}
	}
	// 匹配的url格式:  /welcome?firstname=Jane&lastname=Doe
	apiGroup.GET("/welcome", func(c *gin.Context) {
		firstname := c.DefaultQuery("firstname", "Guest")
		lastname := c.Query("lastname") // 是 c.Request.URL.Query().Get("lastname") 的简写

		c.String(http.StatusOK, "Hello %s %s", firstname, lastname)
	})

	return router
}
