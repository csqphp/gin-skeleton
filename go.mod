module chongwu

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gorm.io/driver/mysql v1.0.4
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/driver/sqlserver v1.0.6
	gorm.io/gorm v1.20.12
	gorm.io/plugin/dbresolver v1.1.0 // indirect
)
