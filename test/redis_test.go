package test

import (
	"chongwu/app/conf"
	"github.com/go-redis/redis"
	"log"
	"testing"
	"time"
)

func TestRedis(t *testing.T) {
	redisdb := redis.NewClient(&redis.Options{
		Addr:     conf.RedisConf["master"].Addr,
		Password: conf.RedisConf["master"].Auth,
		DB:       conf.RedisConf["master"].IndexDb,
	})

	//kv读写
	err := redisdb.Set("key", "value", 1*time.Minute).Err()
	log.Println(err)

	//获取过期时间
	tm, err := redisdb.TTL("key").Result()
	log.Println(tm)

	val, err := redisdb.Get("key").Result()
	log.Println(val, err)

	val2, err := redisdb.Get("missing_key").Result()
	if err == redis.Nil {
		log.Println("missing_key does not exist")
	} else if err != nil {
		log.Println("missing_key", val2, err)
	}
}
